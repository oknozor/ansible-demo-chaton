# Chaton demo deployement

Ce dépôt contient les déployement ansible pour les applications suivantes :
- nginx 
- postgresql 
- openldap 
- nextcloud
- matrix synapse (TODO)
- borg (TODO)

![](schema.png)

## Tester le déploiment localement

Pour tester le déploiement ansible vous aurez besoin de générer un machine virtuelle avec [Vagrant](https://www.vagrantup.com/) et d'installer [Ansible](https://www.ansible.com/) sur votre machine.

1. demarrer les VM Debian
```
vagrant up
```

Pour se connecter en ssh à une vm : 
```
vagrant ssh {vm-name}
```

2. installer la configuration ssh pour ansible
```
vragrant ssh-config > ssh.cfg
```

3. lancer le playbook
```
playbooks $ ansible-playbook playbook.yml -i inventories/vagrant.ini
```

Vous devriez pouvoir utiliser nextlcoud à l'adresse localhost:8080 (user: chaton, mp: chaton)
Attention toutefois le service n'est pas (encore) en HTTPS.  


